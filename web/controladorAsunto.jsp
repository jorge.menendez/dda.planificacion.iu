<%-- 
    Document   : controladorAsunto
    Created on : 12/04/2017, 23:31:04
    Author     : Cristian
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
         <%
            String strOpc = request.getParameter("opc");
            if (strOpc != null) {
                if (strOpc.equals("asunto")) {
                    String strTsk = request.getParameter("tsk");
                    
                    String id = request.getParameter("id");
                    String tipo = request.getParameter("tipo");
                    String nombre = request.getParameter("nombre");
                    String idasunto = request.getParameter("idasunto");
                   
                    if (strTsk.equals("insertar")) {
                        String strJSONData = "{\"strId\":\"" + id + "\",\"strNombre\":\"" + nombre + "\",\"strTipo\":\"" + tipo + "\",\"strIdAsunto\":\"" + idasunto + "\"}";
                          
                        response.sendRedirect("asunto/mdAsunto.jsp?acc=getAsunto&data="+ strJSONData);
                    } else if (strTsk.equals("mostrarAsunto")) {
                        response.sendRedirect("asunto/vwAsunto.jsp");
                    } 
                   else if (strTsk.equals("mostrarError")) {
                        response.sendRedirect("error/vwError.jsp");
                    }
                }
            } else {
                response.sendRedirect("vwPlanClase.jsp");
            }
        %>

        
    </body>
</html>
