<%-- 
    Document   : controladorReporRecurso
    Created on : 04/05/2017, 21:49:59
    Author     : Cristian
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        
          
           <%
            String strOpc = request.getParameter("opc");
            if (strOpc != null) {
                if (strOpc.equals("recursoreporte")) {
                     String strTsk = request.getParameter("tsk");
                     
                    String tipo = request.getParameter("tipo");
                   
                  
                    if (strTsk.equals("mostrarreporte")) {
                        String strJSONData = "{\"strTipo\":\"" + tipo + "\"}";
                        response.sendRedirect("recurso/mdRecursoReporte.jsp?acc=getRecursoReporte&data="+ strJSONData);
                    } else if (strTsk.equals("mostrarRecursoReporte")) {
                        response.sendRedirect("recurso/vwRecursoReporte.jsp");
                    } 
                }
            } 
            else {
                response.sendRedirect("vwPlanClase.jsp");
            }
        %>
        
        
    </body>
</html>
