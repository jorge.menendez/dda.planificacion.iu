<%-- 
    Document   : controladorRecurso
    Created on : 11/04/2017, 22:29:47
    Author     : Cristian
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
      
           <%
            String strOpc = request.getParameter("opc");
            if (strOpc != null) {
                if (strOpc.equals("recurso")) {
                    String strTsk = request.getParameter("tsk");
                    
                    String id = request.getParameter("id");
                    String tipo = request.getParameter("tipo");
                    String detalle = request.getParameter("detalle");
                    String estado = request.getParameter("estado");
                   
                  
                    if (strTsk.equals("insertar")) {
                        String strJSONData = "{\"strId\":\"" + id + "\",\"strTipo\":\"" + tipo + "\",\"strDetalle\":\"" + detalle + "\"}";
                        response.sendRedirect("recurso/mdRecurso.jsp?acc=setRecurso&data="+ strJSONData);
                    }
                    else if (strTsk.equals("cambiarEstado")) {
                        String strJSONData = "{\"strId\":\"" + id + "\",\"strTipo\":\"" + tipo + "\",\"strDetalle\":\"" + detalle + "\",\"strEstado\":\"" + estado + "\"}";
                        response.sendRedirect("recurso/mdRecurso.jsp?acc=cambiarestadoRecurso&data="+ strJSONData);
                    } 
                    else if (strTsk.equals("saveRecursos")) {
                        String strJSONData = request.getParameter("jsonGRecursos");
                        if (strJSONData != null){
                            response.sendRedirect("recurso/mdRecurso.jsp?acc=saveRecursos&data="+ strJSONData);
                        }
                    }
                    else if (strTsk.equals("mostrarRecurso")) {
                        response.sendRedirect("recurso/vwRecurso.jsp");
                    }
                    else if (strTsk.equals("mostrarRecurso1")) {
                        response.sendRedirect("recurso/vwRecurso.jsp");
                    }
                   else if (strTsk.equals("mostrarError")) {
                        response.sendRedirect("error/vwError.jsp");
                    }
                }
                if (strOpc.equals("recursoreporte")) {
                     String strTsk = request.getParameter("tsk");
                     
                    String tipo = request.getParameter("tipo");
                   
                  
                    if (strTsk.equals("mostrarreporte")) {
                        String strJSONData = "{\"strTipo\":\"" + tipo + "\"}";
                        response.sendRedirect("recurso/mdRecursoReporte.jsp?acc=getRecursoReporte&data="+ strJSONData);
                    } else if (strTsk.equals("mostrarRecursoReporte")) {
                        response.sendRedirect("recurso/vwRecursoReporte.jsp");
                    } 
                }
            } else {
                response.sendRedirect("vwPlanClase.jsp");
            }
        %>

    </body>
</html>
