<%-- 
    Document   : controladorCalendario
    Created on : 23/02/2017, 20:15:47
    Author     : Cristian
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%
            String strOpc = request.getParameter("opc");
            if (strOpc != null) {
                if (strOpc.equals("calendario")) {
                    String strTsk = request.getParameter("tsk");
                    String fechai = request.getParameter("fechai");
                    String fechaf = request.getParameter("fechaf");
                    String horai = request.getParameter("horai");
                    String horaf = request.getParameter("horaf");
                    String motivo = request.getParameter("motivo");
                    String documento = request.getParameter("documento");
                    String id = request.getParameter("id");
                    String tipo = request.getParameter("tipo");
                  
                    if (strTsk.equals("insertar")) {
                        String strJSONData = "{\"strfechainicio\":\"" + fechai + "\",\"strfechafin\":\"" + fechaf + "\",\"strhorainicio\":\"" + horai + "\",\"strhorafin\":\"" + horaf + "\",\"strmotivo\":\"" + motivo + "\",\"strdocumento\":\"" + documento + "\",\"strid\":\"" + id + "\",\"strestado\":\"" + tipo + "\"}";
                        response.sendRedirect("calendario/mdCalendario.jsp?acc=getCalendario&data="+ strJSONData);
                    } else if (strTsk.equals("mostrarCalendario")) {
                        response.sendRedirect("calendario/vwCalendario.jsp");
                    } 
                   else if (strTsk.equals("mostrarError")) {
                        response.sendRedirect("error/vwError.jsp");
                    }
                }
            } else {
                response.sendRedirect("vwPlanClase.jsp");
            }
        %>

    </body>
</html>
