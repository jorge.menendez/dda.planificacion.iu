<%-- 
    Document   : mdRecursoReporte
    Created on : 04/05/2017, 21:55:12
    Author     : Cristian
--%>

<%@page import="dda.planclase.comun.cRecurso"%>
<%@page import="dda.planclase.comun.cListaRecurso"%>
<%@page import="com.google.gson.Gson"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
          <%
            try {
                String strAcc = request.getParameter("acc");
                String strData = request.getParameter("data");
                
                if (strAcc.equals("getRecursoReporte")) {
                       Gson gson = new Gson();
                       
        dda.planclase.ws.DdaWSPlanClaseAD_Service service = new dda.planclase.ws.DdaWSPlanClaseAD_Service();
	dda.planclase.ws.DdaWSPlanClaseAD port = service.getDdaWSPlanClaseADPort();
	   // // TODO process result here
                    //cCalendario obj = new cCalendario();
                    //obj.setStrdocumento("nuevo");
                    //obj.setStrhorainicio("12:00");
                    //String    docenteJSON = gson.toJson(obj);
                   cRecurso oRecurso = new cRecurso();
                   oRecurso.setStrTipo("Tema");
                   String json=gson.toJson(oRecurso);
                    
                   java.lang.String result = port.getRecursoSWAD(strData);
      
                   
                   if (result.equals("")) {
                        response.sendError(500, "ERROR: no es posible obtener los datos del calendario " + strData + ".");
                    } else {
  
                        cListaRecurso recursoreporte = gson.fromJson(result, cListaRecurso.class);
                       
                        session.setAttribute("recursoreporte", recursoreporte);
                        response.sendRedirect("../controladorReporRecurso.jsp?opc=recursoreporte&tsk=mostrarRecursoReporte");
                    }

                }
            } catch (Exception ex) {
                // TODO handle custom exceptions here
            }
        %>

        
    </body>
</html>
