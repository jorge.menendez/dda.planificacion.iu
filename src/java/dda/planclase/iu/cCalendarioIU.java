/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dda.planclase.iu;

import dda.planclase.comun.cCalendario;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author Cristian
 */
public class cCalendarioIU {

    private String strid;

    public String getStrid() {
        return strid;
    }

    public void setStrid(String strid) {
        this.strid = strid;
    }
    List<cCalendario> lCalendario = new ArrayList<>();

    public List<cCalendario> getlCalendario() {
        return lCalendario;
    }

    public void setlCalendario(List<cCalendario> lCalendario) {
        this.lCalendario = lCalendario;
    }

    public cCalendarioIU() {
        this.strid = "";
    }

    public String toHTML1() {
        String result = "";
        result += "<h5 align='center'>" + getStrid() + "</h5><br>";
        return result;
    }

    public String toHTML() throws ParseException {
        Iterator it = lCalendario.iterator();
        String result = "";
        result += "<a id='lnkSubir1' class='nav-link text-black fa fa-calendar-plus-o'  data-toggle='tooltip' data-placement='bottom' title='Ingresar Fecha'></a>";
        result += "<center><H2>CALENDARIO</H2></center>";
        result += "<div class='col-xs-12'>" +
"                  <div id='contenidoDinamico' class='mt-2'>";
        String var=lCalendario.get(0).getStrfechainicio();
        int mes=extraerMes(var);
        int anio=extraerAnio(var);
        String fecha=mesAnio(mes,anio);
        result+=fecha+"<br/>";
        while (it.hasNext()) {
            int mesi=0;
            int anioi=0;
            String fechai="";
            int mesf=0;
            int aniof=0;
            String fechaf="";
            cCalendario oCalendario = (cCalendario) it.next();
            String color = "";
            if (Objects.equals(oCalendario.getStrestado(), "Feriado")) {
                color = "#ffff78";
            }
            if (Objects.equals(oCalendario.getStrestado(), "Entrega de Notas")) {
                color = "#fedd8e";
            }
            if (Objects.equals(oCalendario.getStrestado(), "Otros")) {
                color = "#CCFFFF";
            }
            mesi=extraerMes(oCalendario.getStrfechainicio());
            anioi=extraerAnio(oCalendario.getStrfechainicio());
            fechai=mesAnio(mesi,anioi);
            mesf=extraerMes(oCalendario.getStrfechafin());
            aniof=extraerAnio(oCalendario.getStrfechafin());
            fechaf=mesAnio(mesf,aniof);
            if(mes!=mesi)
            {
                mes=mesi;
                result+=fechai+"<br/>"; 
            }
            
            int diai=extraerDia(oCalendario.getStrfechainicio());
            int diaf=extraerDia(oCalendario.getStrfechafin());
            if((diai==diaf)&&(mesi==mesf)&&(anioi==aniof))
            {
                result+="<div class='col-xs-2 col-form-label' align='right'><div style='width: 15px;height: 15px; background: "+color+"'></div></div>";
                result+="<div class='col-xs-10 col-form-label'>"
                     +diai
                     +" | " + oCalendario.getStrmotivo()+"  "
                     +"<i class='fa fa-calendar-minus-o' aria-hidden='true' style='color: red;'></i>"
                     +"</div>";
            }
            else
            {
                result+="<div class='col-xs-2 col-form-label' align='right'><div style='width: 15px;height: 15px; background: "+color+"'></div></div>";
                result+="<div class='col-xs-10 col-form-label'>"
                     +diai+"-"+diaf
                     +" | " + oCalendario.getStrmotivo()+"  "
                     +"<i class='fa fa-file-pdf-o' aria-hidden='true' style='color: gray;'></i>"+"  "
                     +"<i class='fa fa-calendar-minus-o' aria-hidden='true' style='color: red;'></i>"
                     +"</div>";
            }
            /*
            result += "<label class='col-xs-3 col-form-label'>Inicio:</label>";
        result += "</label>"
               + "<div class='col-xs-10 form-control'>"
               + oCalendario.getStrfechainicio() + oCalendario.getStrhorainicio()
               + oCalendario.getStrfechafin() + oCalendario.getStrhorafin()
               + oCalendario.getStrmotivo() + oCalendario.getStrdocumento()
               + "</div>";
        }
        result +="</div>";*/
        }
        result +="</div>";
        return result;
    }
    
    public int extraerDia(String fecha) throws ParseException
    {
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        Calendar c = Calendar.getInstance();
        date = formatter.parse(fecha);
        c.setTime(date);
        int diaMes = c.get(Calendar.DAY_OF_MONTH);
        return diaMes;
    }
    
    public int extraerMes(String fecha) throws ParseException
    {
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        Calendar c = Calendar.getInstance();
        date = formatter.parse(fecha);
        c.setTime(date);
        int diaMes = c.get(Calendar.DAY_OF_MONTH);
        //obtnener el mes;
        int mes = c.get(Calendar.MONTH) + 1;
        return mes;
    }
    
    public int extraerAnio(String fecha) throws ParseException
    {
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        Calendar c = Calendar.getInstance();
        date = formatter.parse(fecha);
        c.setTime(date);
        int anio = c.get(Calendar.YEAR);
        return anio;
    }
    
    public String mesAnio(int m, int a) {
        String fecha = "";
      String[] codDia = {"ene", "feb", "mar", "abr", "may", "jun", "jul", "agost", "sept", "oct", "nov", "dic"};

        fecha = codDia[m - 1] + "," + a;

        return fecha;
    }
}
