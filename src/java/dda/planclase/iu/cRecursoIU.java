/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dda.planclase.iu;

import dda.planclase.comun.cCalendario;
import dda.planclase.comun.cListaRecurso;
import dda.planclase.comun.cRecurso;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author Cristian
 */
public class cRecursoIU {
    
     private List<cRecurso> recurso= new ArrayList<>();

    public List<cRecurso> getRecurso1() {
        return recurso;
    }

    public void setRecurso(List<cRecurso> recurso1) {
        this.recurso = recurso1;
    }

     private String strid;

    public String getStrid() {
        return strid;
    }

    public void setStrid(String strid) {
        this.strid = strid;
    }
     
    public String toHTML1() {
        String result = "";
        result += "<h5 align='center'>" + getStrid() + "</h5><br>";
        return result;
    }
      
     public String toHTML2() {
           
      Iterator it = recurso.iterator();
      String tipo=recurso.get(0).getStrTipo();
        String result = "";
        result+="<label class='col-xs-2 col-form-label'></label><div class='col-xs-5'><input class='form-control' data-container='body' id='detalle' type='text'></div>";
        result+= "<button type='button' class='btn btn-secondary active' id='addRecurso' onclick='clickIngresarRecurso(); return false;'>"
                + "<i class='fa fa-plus-circle'></i>"
                + "Agregar "+tipo+""
                + "</button>";
        while (it.hasNext()) {
           
            cRecurso oRecurso = (cRecurso) it.next();
            if (oRecurso.getStrEstado().equals("H")) {
             result += "<div class='recurso' data-detalle='" + oRecurso.getStrDetalle() + "' data-id='"+oRecurso.getStrId()+"' data-tipo='"+oRecurso.getStrTipo()+"' data-estado='"+oRecurso.getStrEstado()+"'>"
                        + "<div class='input-group'>"
                        + "<input type='hidden' id='tipo' value='"+oRecurso.getStrTipo()+"'>"
                        + "<span class='input-group-addon'>"+oRecurso.getStrTipo()+" </span>"
                        + "<input type='text' class='form-control Recursos' id='" + oRecurso.getStrId() + "' placeholder='Descripci&oacute;n del recurso' value='" + oRecurso.getStrDetalle() + "'/>";
                result += "<span class='input-group-addon' id='deshabilitarRecurso' data-toggle='tooltip' data-placement='bottom' title='Eliminar RECURSO'>"
                        + "<i class='fa fa-minus-circle'> </i>"
                        + "</span>";
                result += "</div>"
                        + "</div>";
            }
            else {
                result += "<div class='recurso' data-detalle='" + oRecurso.getStrDetalle() + "' data-id='"+oRecurso.getStrId()+"' data-tipo='"+oRecurso.getStrTipo()+"' data-estado='"+oRecurso.getStrEstado()+"'>"
                        + "<div class='input-group'>"
                        + "<input type='hidden' id='tipo' value='Recurso'>"
                        + "<span class='input-group-addon'>"+oRecurso.getStrTipo()+" </span>"
                        + "<input type='text' class='form-control Recursos' disabled placeholder='Descripci&oacute;n del recurso' value='" + oRecurso.getStrDetalle() + "'/>";
                result += "<span class='input-group-addon habilitarRecurso' data-toggle='tooltip' data-placement='bottom' title='Habilitar RECURSO' id='" + oRecurso.getStrId() + "'>"
                        + "<input type='checkbox' class='habilitar'>"
                        + "</span>";
                result += "</div>"
                        + "</div>";
            }
      }
        result += "</div>"
                + "</div>"
                + "</div>"
                + "</form>"
                + "<!-- barra de botones -->"
                + "<div class='form-group row'>"
                + "<div class='col-xs-9'>"
                + "</div>"
                + "<div class='col-xs-3'>"
                + "<button type='button' id='btnGuardar' class='btn btn-secondary float-xs-right gRecursos' data-toggle='tooltip' data-placement='top' title='Guardar cambios'>"
                + "Guardar | <i class='fa fa-fw'></i>"
                + "</button>"
                + "</div>"
                + "</div>";
     return result;
         }
     
     public String toHTML3() {
        String result = "";
        result += "<h5 align='center'>" + getStrid() + "</h5><br>";
        return result;
    }
       
 }
    
